import React, { useEffect, useState } from 'react'
import "./cards.scss";
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from "@mui/material/CardMedia";
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import PeopleAltOutlinedIcon from "@mui/icons-material/PeopleAltOutlined";
import SettingsIcon from "@mui/icons-material/Settings";
import Grid from '@mui/material/Grid';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';


import { Link } from "react-router-dom";
import { collection, getDocs, deleteDoc, doc } from "firebase/firestore";
import { db } from "../../firebase"

const Cards = () => {
  const [data, setData] = useState([]);

  console.log(data)
  useEffect(()=>{
    const fetchData = async()=>{
      let list = []
      try{
        const querySnapshot = await getDocs(collection(db, "cars"));
        querySnapshot.forEach((doc) => {
          list.push({ id: doc.id, ...doc.data()})
        });
        setData(list)
      }catch(err){
        console.log(err)
      }
    }
    fetchData()
  }, [])

  const handleDelete = async(id) => {
    try{
      await deleteDoc(doc(db, "cars" , id));
      setData(data.filter((item) => item.id !== id));
    }catch(err){
      console.log(err)
    }
  };

  return (
    <div className="datatable">
      <div className="datatableTitle" >
        <div style={{margin:"0 2rem"}}>
        List Car
        </div>
        <Link to="/cars/add" className="link">
          + Add New Car
        </Link>
      </div>
      
      <div className='cards-margin'>
      {data.map((item)=>{
       return <div className='Cards'> 
        <Box sx={{ minWidth: 250 }}>
        <Grid item xs={4}>
          <Card variant="outlined" sx={{ width: 250, ml: 6, px: 3, py: 1, boxShadow: 2, mt: 5 }} >
            <CardMedia
            component="img"
            height="250"
            width="250"
            style={{objectFit:"cover"}}
            image={item.img}
            alt="Foto Mobil"
            />

            <CardContent>
              <Typography gutterBottom variant="subtitle1" component="div">
                {item.namaMobil}
              </Typography>
              <Typography
                variant="h6"
                color="text.secondary"
                sx={{ fontWeight: "bold" }}
              >
                Rp. {item.harga} / hari
              </Typography>
              <Typography gutterBottom variant="subtitle1" component="div">
                <CalendarMonthIcon />
                {item.startRent} - {item.endRent}
              </Typography>
              <Typography gutterBottom variant="subtitle1" component="div">
                <SettingsIcon />
                Manual
              </Typography>
            </CardContent>

            <CardActions >
              <Button
                variant="outlined"
                color="error"
                sx={{ mb: 4, ml: 1, width: 300 }}
                onClick={() => handleDelete(item.id)}
              >
                Delete
              </Button>
              <Button
                variant="contained"
                color="success"
                sx={{ mb: 4, ml: 1, width: 300 }}
                onClick={() => handleDelete(item.id)}
              >
                Edit
              </Button>
            </CardActions>    
          </Card>
        </Grid>
        </Box>
      </div>
    })}
    </div>
    </div>
  );
};

export default Cards;