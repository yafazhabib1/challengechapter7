import React from 'react'
import './sidebar.scss';
import AddBusinessIcon from '@mui/icons-material/AddBusiness';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import LogoutOutlinedIcon from '@mui/icons-material/LogoutOutlined';
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import SquareIcon from '@mui/icons-material/Square';
import HomeRounded from "@mui/icons-material/HomeRounded";
import LocalShippingOutlinedIcon from '@mui/icons-material/LocalShippingOutlined';

import { Link } from "react-router-dom";

const Sidebar = () => {
  return (
    <div className='sidebar'>
        <div className='top'>
        <Link to="/" style={{ textDecoration: "none" }}>
          <span className="logo"><SquareIcon/></span>
        </Link>        
        </div>
        <hr />
        <div className='center'>
            <ul>
                <Link to="/" style={{ textDecoration: "none" }}>
                    <li>
                        <HomeRounded className='icon'/>
                        <span>Dashboard</span>
                    </li>
                </Link>
                <Link to="/cars" style={{ textDecoration: "none" }}>
                    <li>
                        <LocalShippingOutlinedIcon className='icon'/>
                        <span>Cars</span>
                    </li>
                </Link>
                <Link to="/users" style={{ textDecoration: "none" }}>
                  <li>
                    <PersonOutlineIcon className="icon" />
                    <span>List Order</span>
                  </li>
                </Link>
                {/* <p className="title">LISTS</p>
                <p className="title">USER</p>
                <li>
                    <AccountCircleOutlinedIcon className='icon'/>
                    <span>Profile</span>
                </li>
                <li>
                    <LogoutOutlinedIcon className='icon'/>
                    <span>Logout</span>
                </li> */}
            </ul>
        </div>
        {/* <div className='bottom'>
            <div className="colorOption"></div>
            <div className="colorOption"></div>
        </div> */}
    </div>
  )
}

export default Sidebar