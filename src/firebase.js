import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore"
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyCts25jH88Cu3R5gaZvgz-UqiPYpEhi1i8",
  authDomain: "tutorial-86992.firebaseapp.com",
  projectId: "tutorial-86992",
  storageBucket: "tutorial-86992.appspot.com",
  messagingSenderId: "616359445095",
  appId: "1:616359445095:web:31723b7049e73a3ee30ac2"
};

const app = initializeApp(firebaseConfig);
export const db = getFirestore(app)
export const auth = getAuth()
export const storage = getStorage(app);