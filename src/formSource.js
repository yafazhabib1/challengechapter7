export const userInputs = [
    {
      id: "username",
      label: "Username",
      type: "text",
      placeholder: "unisbadri",
    },
    {
      id: "displayName",
      label: "Name",
      type: "text",
      placeholder: "Unis Badri",
    },
    {
      id: "email",
      label: "Email",
      type: "mail",
      placeholder: "unisbadri@gmail.com",
    },
    {
      id: "phone",
      label: "Phone",
      type: "text",
      placeholder: "+62 822 xxxx xxxx",
    },
    {
      id: "password",
      label: "Password",
      type: "password",
    },
    {
      id: "address",
      label: "Address",
      type: "text",
      placeholder: "Jl. Cemara No.42, Peninggilan, Tangerang",
    },
    {
      id: "country",
      label: "Country",
      type: "text",
      placeholder: "Indonesia",
    },
  ];

export const carsInputs = [
    {
      id: "namaMobil",
      label: "Car Name",
      type: "text",
      placeholder: "Nama Mobil",
    },
    {
      id: "harga",
      label: "Car Rental Prices",
      type: "number",
      placeholder: "300.000",
    },
    {
      id: "startRent",
      label: "Rental Start Date",
      type: "text",
      placeholder: "14 Februari 2022",
    },
    {
      id: "endRent",
      label: "Rental End Date",
      type: "text",
      placeholder: "22 Juli 2022",
    },
  ];