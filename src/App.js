import {
  BrowserRouter,
  Routes,
  Route,
  Navigate
} from "react-router-dom";
import Home from "./pages/home/Home";
import Cars from "./pages/cars/Cars"
import Login from "./pages/login/Login";
import List from "./pages/list/List";
import New from "./pages/new/New";
import Add from "./pages/addcars/newCars";
import { useContext } from 'react'
import { AuthContext } from './context/AuthContext';
import { userInputs, carsInputs } from "./formSource";

function App() {

  const {currentUser} = useContext(AuthContext)

  const RequireAuth = ({children}) => {
    return currentUser ? (children) : <Navigate to="/login"/>
  }

  return (
    <div className="App">
      <BrowserRouter>
      <Routes>
        <Route path="/">
          <Route path='login' element={<Login/>}/>
          <Route index element={
            <RequireAuth>
              <Home/>
            </RequireAuth>
          } />
          <Route path='users'>
            <Route index element={<List/>} />
            <Route
                path="new"
                element={<New inputs={userInputs} title="Add New User" />}
            />
          </Route>
          <Route path='cars'>
            <Route index element={<Cars/>} />
            <Route
                path="add"
                element={<Add inputs={carsInputs} title="Add New Cars" />}
            />
          </Route>
        </Route>
      </Routes>
    </BrowserRouter>
    </div>
  );
}

export default App;
