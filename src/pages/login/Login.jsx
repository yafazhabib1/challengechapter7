import React, { useContext, useState } from 'react'
import "./login.scss"
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../../firebase"
import { useNavigate } from "react-router-dom"
import { AuthContext } from '../../context/AuthContext';
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import imgLogin from "../images/login.png";

// import { BsGoogle, BsFacebook, BsGithub } from "react-icons/bs";

const Login = () => {
  const [error, setError] = useState(false)
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  const navigate = useNavigate()

  const {dispatch} = useContext(AuthContext)

  const handleLogin = (e) => {
    e.preventDefault()
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Signed in 
        const user = userCredential.user;
        dispatch({type:"LOGIN", payload:user})
        navigate("/")
      })
      .catch((error) => {
        setError(true)
      });
    }

  return (
    <div className="Login" style={{ display: "flex" }}>
      <Box
        component="img"
        sx={{
          display: "flex",
          width: "70%",
        }}
        alt="Car Rental"
        src={imgLogin}
      />
      <div className="form" style={{ padding: "3rem" }}>
        <Box sx={{ mt: "4rem" }}>
          <h1 style={{ fontSize: "30px", fontWeight: "bolder" }}>Welcome, Admin BCR</h1>
        </Box>
        <div className='login'>
        <form onSubmit={handleLogin}>
        <input type="email" placeholder='email' onChange={e=>setEmail(e.target.value)} />
        <input type="password" placeholder='password' onChange={e=>setPassword(e.target.value)} />
        <button type='submit'>Sign In</button>
        {error && <span>Wrong Email or Password!</span>}
        </form>
      </div>
    </div>
    </div>
  )
}

export default Login