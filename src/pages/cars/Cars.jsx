import React from 'react'
import "./cars.scss";
import Sidebar from '../../components/sidebar/Sidebar'
import Navbar from '../../components/navbar/Navbar';
import Cards from '../../components/cards/Cards';
import Grid from '@mui/material/Grid';

const Cars = () => {
      return (
        <div className="cars">
          <Sidebar />
          <div className="homeContainer">
              <Navbar/>
              {/* <div className='cardsContainer'>
                <Cards/>
              </div> */}
              <Grid container spacing={3} sx={{
                display:'flex', justifyContent:'center', paddingTop:'50px'
                }}>
                    <Cards/>
              </Grid>
          </div>
        </div>
      )
}

export default Cars