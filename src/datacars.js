export const userColumns = [
    { field: "id", headerName: "ID", width: 70 },
    {
      field: "namaMobil",
      headerName: "Mobil",
    //   width: 230,
      renderCell: (params) => {
        return (
          <div className="cellWithImg">
            <img className="cellImg" src={params.row.img} alt="avatar" />
            {params.row.username}
          </div>
        );
      },
    },
    {
      field: "harga",
      headerName: "Harga",
    //   width: 230,
    },
    {
      field: "startRent",
      headerName: "startRent",
    },
    {
      field: "endRent",
      headerName: "endRent",
    },
  ];
  